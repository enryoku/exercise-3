﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_3
{
    public partial class myExercise03 : Form
    {
        public myExercise03()
        {
            InitializeComponent();
        }

        private void OutputWeight_TextChanged(object sender, EventArgs e)
        {

        }

        private void InputWeight_TextChanged(object sender, EventArgs e)
        {

        }

        private void ConvertButton_Click(object sender, EventArgs e)
        {
                try {
                    double earthWeight;
                    double marsWeight;
                    earthWeight = float.Parse(inputWeight.Text);
                    marsWeight = earthWeight / 9.81 * 3.711;
                    marsWeight = Math.Round(marsWeight, 3);
                    outputWeight.ReadOnly = true;
                    outputWeight.Text = marsWeight.ToString();
                }
                catch(FormatException)
                {
                    MessageBox.Show("Please enter an Earth weight.", "Information Required");
                }
            {

            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            inputWeight.Text = null;
            outputWeight.Text = null;
        }
    }
}
