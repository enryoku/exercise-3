﻿namespace Exercise_3
{
    partial class myExercise03
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputWeight = new System.Windows.Forms.TextBox();
            this.outputWeight = new System.Windows.Forms.TextBox();
            this.weightOnEarthLabel = new System.Windows.Forms.Label();
            this.weightOnMars = new System.Windows.Forms.Label();
            this.convertButton = new System.Windows.Forms.Button();
            this.titleLabel = new System.Windows.Forms.Label();
            this.resetButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // inputWeight
            // 
            this.inputWeight.Location = new System.Drawing.Point(206, 41);
            this.inputWeight.MaxLength = 12;
            this.inputWeight.Name = "inputWeight";
            this.inputWeight.Size = new System.Drawing.Size(100, 20);
            this.inputWeight.TabIndex = 0;
            this.inputWeight.TextChanged += new System.EventHandler(this.InputWeight_TextChanged);
            // 
            // outputWeight
            // 
            this.outputWeight.Location = new System.Drawing.Point(206, 100);
            this.outputWeight.Name = "outputWeight";
            this.outputWeight.Size = new System.Drawing.Size(100, 20);
            this.outputWeight.TabIndex = 1;
            this.outputWeight.TextChanged += new System.EventHandler(this.OutputWeight_TextChanged);
            // 
            // weightOnEarthLabel
            // 
            this.weightOnEarthLabel.AutoSize = true;
            this.weightOnEarthLabel.Location = new System.Drawing.Point(9, 44);
            this.weightOnEarthLabel.Name = "weightOnEarthLabel";
            this.weightOnEarthLabel.Size = new System.Drawing.Size(135, 13);
            this.weightOnEarthLabel.TabIndex = 2;
            this.weightOnEarthLabel.Text = "Enter your weight on Earth:";
            // 
            // weightOnMars
            // 
            this.weightOnMars.AutoSize = true;
            this.weightOnMars.Location = new System.Drawing.Point(9, 107);
            this.weightOnMars.Name = "weightOnMars";
            this.weightOnMars.Size = new System.Drawing.Size(107, 13);
            this.weightOnMars.TabIndex = 3;
            this.weightOnMars.Text = "Your weight on Mars:";
            // 
            // convertButton
            // 
            this.convertButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.convertButton.Location = new System.Drawing.Point(231, 146);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(75, 23);
            this.convertButton.TabIndex = 4;
            this.convertButton.Text = "Convert";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.ConvertButton_Click);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(0, 9);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(317, 13);
            this.titleLabel.TabIndex = 5;
            this.titleLabel.Text = "Type in your weight and click convert to see your weight on Mars!";
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(12, 146);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 23);
            this.resetButton.TabIndex = 6;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // myExercise03
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(318, 181);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.convertButton);
            this.Controls.Add(this.weightOnMars);
            this.Controls.Add(this.weightOnEarthLabel);
            this.Controls.Add(this.outputWeight);
            this.Controls.Add(this.inputWeight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "myExercise03";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercise 3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputWeight;
        private System.Windows.Forms.TextBox outputWeight;
        private System.Windows.Forms.Label weightOnEarthLabel;
        private System.Windows.Forms.Label weightOnMars;
        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Button resetButton;
    }
}

